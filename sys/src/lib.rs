pub mod proto {
	tonic::include_proto!("openmatch");
}

pub mod messages {
	include!(concat!(env!("OUT_DIR"), "/includes/messages.rs"));
	pub use crate::proto::double_range_filter;
}

#[cfg(feature = "frontend")]
pub mod frontend {
	include!(concat!(env!("OUT_DIR"), "/includes/frontend.rs"));
	
	#[doc(hidden)]
	pub mod service {
		#[cfg(feature = "client")]
		pub use crate::proto::frontend_service_client;
		#[cfg(feature = "server")]
		pub use crate::proto::frontend_service_server;
	}
}

#[cfg(feature = "backend")]
pub mod backend {
	include!(concat!(env!("OUT_DIR"), "/includes/backend.rs"));
	
	#[doc(hidden)]
	pub mod service {
		#[cfg(feature = "client")]
		pub use crate::proto::backend_service_client;
		#[cfg(feature = "server")]
		pub use crate::proto::backend_service_server;
	}
}

#[cfg(feature = "function")]
pub mod function {
	include!(concat!(env!("OUT_DIR"), "/includes/matchfunction.rs"));
	
	#[doc(hidden)]
	pub mod service {
		#[cfg(feature = "client")]
		pub use crate::proto::match_function_client;
		#[cfg(feature = "server")]
		pub use crate::proto::match_function_server;
	}
}

#[cfg(feature = "evaluator")]
pub mod evaluator {
	include!(concat!(env!("OUT_DIR"), "/includes/evaluator.rs"));
	
	#[doc(hidden)]
	pub mod service {
		#[cfg(feature = "client")]
		pub use crate::proto::evaluator_client;
		#[cfg(feature = "server")]
		pub use crate::proto::evaluator_server;
	}
}

#[cfg(feature = "query")]
pub mod query {
	include!(concat!(env!("OUT_DIR"), "/includes/query.rs"));
	
	#[doc(hidden)]
	pub mod service {
		#[cfg(feature = "client")]
		pub use crate::proto::query_service_client;
		#[cfg(feature = "server")]
		pub use crate::proto::query_service_server;
	}
}
