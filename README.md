# open-match-rs

Rust bindings to the [open-match API](https://github.com/googleforgames/open-match/tree/master/api)